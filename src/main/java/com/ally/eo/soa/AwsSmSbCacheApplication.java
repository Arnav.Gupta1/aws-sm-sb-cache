package com.ally.eo.soa;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueResponse;
import software.amazon.awssdk.services.secretsmanager.model.SecretsManagerException;

@SpringBootApplication
@RestController
public class AwsSmSbCacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsSmSbCacheApplication.class, args);
	}

	@GetMapping("/token")
	@Cacheable("tokenCache")
	public String getAWSToken() {
		// AWS Retrieve Token -- START
		String secretName = "";// Secret Manager Name created via GitLab pipeline.
		Region region = Region.US_EAST_1;

		// The below are credentials for AWS VPC user that will be created in DFS Account
		String accessKeyId = "<<PASTE YOUR ACCESS ID HERE>>";
		String secretAccessKey = "<<PASTE YOUR ACCESS KEY HERE>>";

		Instant starttime = Instant.now();

		AwsBasicCredentials creds = AwsBasicCredentials.create(accessKeyId, secretAccessKey);

		SecretsManagerClient secretsClient = SecretsManagerClient.builder().region(region)
				.credentialsProvider(StaticCredentialsProvider.create(creds)).build();

		String token = "";
		try {
			GetSecretValueRequest valueRequest = GetSecretValueRequest.builder().secretId(secretName).build();
			GetSecretValueResponse valueResponse = secretsClient.getSecretValue(valueRequest);
			String secret = valueResponse.secretString();
			ObjectMapper mapper = new ObjectMapper();
			Map<String, String> map = mapper.readValue(secret, Map.class);
			token = map.get("token");
			secretsClient.close();
		} catch (SecretsManagerException e) {
			System.err.println(e.awsErrorDetails().errorMessage());
			System.exit(1);
		}

		catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		// AWS Retrieve Token -- END

		Instant endtime = Instant.now();

		System.out.println("Time Taken ::: " + ChronoUnit.MILLIS.between(starttime, endtime) + " milliseconds");
		System.out.println("Time Taken ::: " + ChronoUnit.MICROS.between(starttime, endtime) + " microseconds");
		System.out.println("Time Taken ::: " + ChronoUnit.SECONDS.between(starttime, endtime) + " seconds");
		System.out.println("---------------------------------------------------------------------");

		return token;
	}

}
