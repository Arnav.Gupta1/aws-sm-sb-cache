/**
 * 
 */
package com.ally.eo.soa;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;

/**
 * @author gzrcx0
 *
 */
public class CacheEventLogger implements CacheEventListener<Object, Object> {

	@Override
	public void onEvent(CacheEvent<? extends Object, ? extends Object> event) {
//		System.out.println("On Event:: " + event.getOldValue() + "::"+ event.getNewValue());
//		System.out.println("---------------------------------------------------------------------");
	}
}
